import { createStore, applyMiddleware } from "redux"
import{composeWithDevTools} from 'redux-devtools-extension'
import thunk from "redux-thunk"
import { createWrapper } from "next-redux-wrapper"
import reducers from "./reducers"

const middlewares = [thunk]

const makeStore = () => createStore(reducers, composeWithDevTools(applyMiddleware(...middlewares)))

export const wrapper = createWrapper(makeStore)