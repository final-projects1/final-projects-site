import types from './types';

export const getInternships = payload => ({
  type: types.GET_INTERNSHIPS,
  payload,
});
