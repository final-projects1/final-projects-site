import types from './types';

const internshipsReducer = (state = null, action) => {
  switch (action.type) {
    case types.GET_INTERNSHIPS:
      return action.payload;
    default:
      return state;
  }
};
export default internshipsReducer;
