const types = {
    GET_METADATA: '[METADATA] GET',
    SET_CURRENT_YEAR: '[METADATA] SET_YEAR'
    
};

export default types;