import types from './types';

const initialState = {
  hideNavbar: false,
  hideFooter: false,
  currentYear: null,
  years: [],
  winnerProjectTypes: []
};

const metaData = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_METADATA:
      return {
        ...state,
        ...action.payload
      };
    case types.SET_CURRENT_YEAR:
      return{
        ...state,
        currentYear: action.payload
      }
    default:
      return state;
  }
};

export default metaData;
