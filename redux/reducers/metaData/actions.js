import types from './types';

export const getMetaDataAction = payload => ({
  type: types.GET_METADATA,
  payload,
});

export const setCurrentYear = year => ({
    type: types.SET_CURRENT_YEAR,
    payload: year,
})