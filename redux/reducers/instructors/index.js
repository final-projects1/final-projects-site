import types from './types';

const instructorsReducer = (state = null, action) => {
  switch (action.type) {
    case types.GET_INSTRUCTORS:
      return action.payload;
    default:
      return state;
  }
};
export default instructorsReducer;
