import types from './types';

export const getInstructors = payload => ({
    type: types.GET_INSTRUCTORS,
    payload
  });