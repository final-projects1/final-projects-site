import types from './types';

const initialState = {
  isOpen: false,
  type: null,
  data: null
};

const modal = (state = initialState, action) => {
  switch (action.type) {
    case types.OPEN_MODAL:
      return {
          isOpen: true,
          ...action.payload
      };
    case types.CLOSE_MODAL:
      return {
        isOpen: false,
        type: null,
        data: null
      };
    default:
      return state;
  }
};
export default modal;
