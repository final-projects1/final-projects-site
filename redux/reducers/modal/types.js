const types = {
    OPEN_MODAL: '[MODAL] OPEN',
    CLOSE_MODAL: '[MODAL] CLOSE'

};

export default types;