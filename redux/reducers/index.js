import { combineReducers } from "redux";
import instructors from "./instructors";
import internships from "./internships";
import projects from "./projects";
import metaData from './metaData'
import modal from './modal'

const reducers = combineReducers({
    instructors,
    internships,
    projects,
    metaData,
    modal
});

export default reducers;