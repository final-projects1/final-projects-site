import types from './types';

export const getProjects = payload => ({
    type: types.GET_PROJECTS,
    payload
  });