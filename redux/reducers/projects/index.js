import types from './types';

const projectsReducer = (state = null, action) => {
  switch (action.type) {
    case types.GET_PROJECTS:
      return action.payload?? null;
    default:
      return state;
  }
};
export default projectsReducer;
