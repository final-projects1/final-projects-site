import Link from 'components/Link';
import Dropdown from 'components/Dropdown';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import {useCallback, useEffect, useMemo} from 'react';
import { setCurrentYear } from 'redux/reducers/metaData/actions';
import { useRouter } from 'next/router';
class navLink {
  constructor(linksName, title, href, Component, options, defaultTitle) {
    this.links = linksName;
    this.title = title;
    this.defaultTitle = defaultTitle;
    this.href = href;
    this.Component = Component;
    this.options = options;
  }
}
const pages = [
  {
    links: null,
    title: 'Home',
    href: '/',
    Component: Link,
  },
  {
    links: null,
    title: 'Instructors',
    href: '/instructors',
    Component: Link,
  },
  {
    linksName: 'internshipsDropdown',
    title: 'Internships',
    href: '/internships',
    Component: Dropdown,
  },
  {
    linksName: null,
    title: 'Winners Projects',
    href: '/winners',
    Component: Link,
  },
  {
    linksName: 'yearsDropdown',
    title: '',
    defaultTitle: 'currentYear',
    href: '/',
    Component: Dropdown,
  },
];

export default function useNavbarLinks() {
  const router = useRouter();

  //change data by year
  const onClickYear = useCallback(
    option => {
      setCurrentYear(option);
      const { replace } = router;
      replace(`/${option}`);
    }
  );

  const { internships, years, restMetadata } = useSelector(
    ({ internships, metaData: {years, ...restMetadata} }) => ({ internships, years, restMetadata }),
    shallowEqual,
  );

  const internshipsDropdown = useMemo(
    () =>
      internships &&
      internships.map(
        intern =>
          new navLink(
            null,
            intern.name,
            `/internships/${intern.name.replaceAll(' ', '-')}`,
            'p',
            {
              onClick: () => {
                router.replace(`/${restMetadata.currentYear}/internships/${intern.name.replaceAll(' ', '-')}`)
              },
            },
          ),
      ),
    [internships],
  );
  const yearsDropdown = useMemo(
    () =>
      years &&
      years.map(
        year =>
          new navLink(null, year, '/', 'p', {
            onClick: onClickYear,
          }),
      ),
    [years],
  );

  const dropdownLinks = useMemo(
    () => ({ internshipsDropdown, yearsDropdown }),
    [yearsDropdown, internshipsDropdown]
  );

  const links = useMemo(
    () =>
      pages &&
      pages.map(({ linksName, title, href, Component , defaultTitle}) => {
        return new navLink(
          linksName && dropdownLinks[linksName],
          title,
          href,
          Component,
            {},
            defaultTitle && restMetadata[defaultTitle],
        );
      }),
    [dropdownLinks, restMetadata],
  );

  return links;
}
