import { useMemo } from 'react';

const defaults = {
  projects: 'default-project-img.jpg',
  instructors: 'default-instructor-img.jpg',
};

export default function useResources(path, imgName, currentYear) {

  return useMemo(
    () =>
      require(`../db/2022/storage/${path}/${imgName || defaults[path]}`),
    [imgName, path],
  );
}
