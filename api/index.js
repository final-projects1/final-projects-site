import axios from 'axios';

const apiService = axios.create({
    baseURL: '/api',
})

export const getMetadata = async year => await apiService.get(`/${year}/metadata`);

export const getAllData = async year => await apiService.get(`/${year}`);

export const getProjectsBy = async (year,data) => await apiService.post(`/${year}/projects`, data);

export const getWinnersProjects = async (year, data) => await apiService.post(`/${year}/winnerProjects`,  data);

apiService.interceptors.response.use(res => {
    if (res.status >= 400) {
        return window.open('/', '_self');
    }

    return res.data;
});

export default apiService;