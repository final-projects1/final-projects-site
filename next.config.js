module.exports = {
  reactStrictMode: true,
  images: {
    loader: 'akamai',
    path: '/',
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: `/${new Date().getFullYear().toString()}`,
        permanent: false
      }
    ]
  }
}
