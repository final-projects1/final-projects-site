import React, {useEffect} from "react";
import { wrapper } from "../redux/store";
import PropTypes from 'prop-types'

import 'styles/globals.css'
import { getAllData, getMetadata } from "api";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import { getInstructors } from "redux/reducers/instructors/actions";
import { getInternships } from "redux/reducers/internships/actions";
import { getMetaDataAction } from "redux/reducers/metaData/actions";

import Navbar from "components/Navbar";
import BasicModal from "components/Modal";
import {useRouter} from "next/router";

function Layout({ children }) {
  return (
    <>
      <Navbar />
      <main>{children}</main>
      <BasicModal />
    </>
  )
}

const MyApp = ({ Component, pageProps}) => {
    const dispatch = useDispatch();
    const { query: { year } } = useRouter();
    const currentYear = useSelector(({ metaData }) => metaData.currentYear, shallowEqual);

  useEffect(() => {
      year && getMetadata(year).then(metadata => {
          dispatch(getMetaDataAction(metadata))
    });
  }, [year]);

  useEffect(() => {
      currentYear && getAllData(year).then(res => {
          dispatch(getInstructors(res.instructors));
          dispatch(getInternships (res.internships));
      }).catch(() => window.open('/', '_self'));
  }, [currentYear]);

  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  )
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};

export default wrapper.withRedux(MyApp);