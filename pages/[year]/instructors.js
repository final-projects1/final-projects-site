import { useSelector } from 'react-redux';
import InstructorsList from 'components/InstructorsList';
import Container from 'components/Container';

function Instructors() {  
  const instructors = useSelector((state) => state.instructors)
  return (
    <Container>
    {instructors && <InstructorsList instructors={instructors}/>}
    </Container>
  );
}

export default Instructors;
