import { useEffect, useMemo } from "react";
import Heading from "components/Heading";
import Text from "components/Text";
import Image from "components/Image";

import styles from "styles/ProjectPage.module.css";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { getProjectsBy } from "../../../api";
import { getProjects } from "../../../redux/reducers/projects/actions";

export default function Project() {
  const dispatch = useDispatch();
  const {
    query: { projectId },
  } = useRouter();
  const { projects, instructors, internships, currentYear } = useSelector(
    ({ instructors, projects, internships, metaData: { currentYear } }) => ({
      instructors,
      projects,
      internships,
      currentYear,
    }),
    shallowEqual
  );
  const project = useMemo(
    () =>
      projects &&
      projectId &&
      projects.find((item) => item.projectId === projectId),
    [projectId, projects]
  );
  const instructorName = useMemo(
    () =>
      instructors &&
      project &&
      instructors.find(({ id }) => id === project.instructorId)?.name,
    [project, instructors]
  );
  const internshipName = useMemo(
    () =>
      internships &&
      project &&
      internships.find(({ id }) => id === project.internshipId)?.name,
    [project, internships]
  );

  useEffect(() => {
    !projects &&
      currentYear &&
      getProjectsBy(currentYear, { projectId: projectId }).then(
        (res) => res && res.length && dispatch(getProjects(res))
      );
  }, [currentYear]);

  return (
    <>
      {project && (
        <div className={styles["project-container"]}>
          <Image
            imgName={project.image}
            path="projects"
            alt={project.name}
            width="400px"
            height="480px"
          />
          <div className={styles["project-section"]}>
            <Heading level="2">{project.name} </Heading>
            <p>{project.description} </p>
            <p>
              <span className={styles["title"]}>Internship: </span>
              {internshipName || "Unknown"}{" "}
            </p>
            <p>
              <span className={styles["title"]}>Instructor: </span>
              {instructorName || "Unknown"}{" "}
            </p>
            <div className={styles["members-container"]}>
              <span className={styles["title"]}>Members :</span>
              {project.members &&
                project.members.map((member) => {
                  return (
                    <div
                      key={member.name}
                      className={styles["member-container"]}
                    >
                      <Text as="span">{member.name}</Text>
                      <a
                        className={styles["link"]}
                        href={`mailto:${member.email}`}
                      >
                        {member.email}
                      </a>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      )}
    </>
  );
}
