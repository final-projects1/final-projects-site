import React, { useEffect, useMemo } from "react";
import styles from "styles/Internships.module.css";

import Container from "components/Container";
import ProjectList from "components/ProjectList";
import { useRouter } from "next/router";
import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { getProjectsBy } from "api";
import { getProjects } from "redux/reducers/projects/actions";
import Image from "components/Image";

export default function Internship() {
  const dispatch = useDispatch();
  const {
    query: { internshipName },
      reload,
      pathname
  } = useRouter();

  const { currentYear, internships, projects } = useSelector(
    ({ metaData: { currentYear }, internships, projects }) => ({
      currentYear,
      internships,
      projects,
    }),
    shallowEqual
  );

  const internshipId = useMemo(
    () =>
      internships &&
      internships.find(
        (item) => item.name == internshipName.replaceAll("-", " ")
      ).id,
    [internshipName, internships]
  );

  useEffect(() => {
    internshipId &&
      getProjectsBy(currentYear, { internshipId: internshipId }).then((res) => {
        dispatch(getProjects(res));
      });
  }, [internshipId]);

  return (
    <Container>
      {internshipName && (
        <div className={styles["internships_img"]}>
          <Image
            path="internships"
            imgName={`${internshipName.toLowerCase()}.jpg`}
            alt={internshipName}
            width={150}
            height={80}
          />
        </div>
      )}
      <ProjectList
        projects={projects}
        title={internshipName ? internshipName.replaceAll("-", " ") : ""}
      />
    </Container>
  );
}
