import { useEffect } from 'react';
import MediaCarousel from 'components/MediaCarousel';
import Container from 'components/Container';
import ProjectList from 'components/ProjectList';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { getProjectsBy } from 'api';
import { getProjects } from 'redux/reducers/projects/actions';

export default function Home() {
  const dispatch = useDispatch();

  const { currentYear, projects } = useSelector(
    ({ metaData: { currentYear }, projects }) => ({
      currentYear,
      projects,
    }),
  );

  useEffect(() => {
    currentYear && getProjectsBy(currentYear).then(res => {
      dispatch(getProjects(res));
    });
  }, [currentYear]);

  return (
    <Container>
      <MediaCarousel />
      {projects && <ProjectList projects={projects} />}
    </Container>
  );
}
