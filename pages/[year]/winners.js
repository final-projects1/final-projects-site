import { useEffect } from 'react';
import styles from 'styles/WinningProjects.module.css';
import Container from 'components/Container';
import { useDispatch, useSelector } from 'react-redux';
import ProjectList from 'components/ProjectList';
import { getWinnersProjects } from '../../api';
import { getProjects } from '../../redux/reducers/projects/actions';
import { GiPodium } from 'react-icons/gi';
import Heading from "components/Heading";

export default function Winners() {
  const dispatch = useDispatch();
  const { currentYear, projects } = useSelector(
    ({ metaData: { winnerProjectTypes, currentYear }, projects }) => ({
      currentYear,
      projects,
    }),
  );

  const _getWinnersProjects = async () => {
    return await getWinnersProjects(currentYear, ['projectWinnerId']);
  };

  useEffect(() => {
    (async () => currentYear &&
      await _getWinnersProjects().then(res => {
        dispatch(getProjects(res));
      }))();
  }, [currentYear]);

  return (
    <Container>
        {
            (projects && projects.length)
            ? <ProjectList projects={projects} />
            : <div className={styles['winning-project-none']}>
                    <span className={styles['winning-project_icon']}>{GiPodium()}</span>
                    <Heading className={styles['winning-project-none--title']}>No winning Projects yet</Heading>
                </div>
        }
    </Container>
  );
}
