import React from "react";
import FromToJson from "components/FormToJson";
import Container from "components/Container";
import { TextField } from "@mui/material";
import Dropdown from "components/Form/Dropdown";

const formItems = [
  {
    name: "year",
    placeholder: "Year",
    Component: Dropdown,
    items: [{name:'2021', value: "2021"},{name:'2022', value: "2022"}],
    isDropdown : true
  },
  {
    name: "projectId",
    placeholder: "Project Id",
    component: TextField,
  },
  {
    name: "projectName",
    placeholder: "Project Name",
    component: TextField
  },
  {
    name: "description",
    placeholder: "Descripion",
    component: TextField
  },
  {
    name: "image",
    placeholder: "Image",
    component: TextField
  },
  {
    name: "internshipName",
    placeholder: "Internship Name",
    component: TextField
  },
  {
    name: "instructorId",
    placeholder: "Instructor Name",
    component: TextField
  },
  {
    name: "members",
    placeholder: "Members",
    component: TextField
  }
];

function projectForm()  {
  return <Container> <FromToJson formData={formItems}/></Container>;
};
export default projectForm;