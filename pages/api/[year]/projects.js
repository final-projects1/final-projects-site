import ProjectModel from "../../../backend/db/models/Project";

export default async function projects(req, res) {
    try {
        if (req.method === 'POST') {
            const queries = req.body;
            const { year } = req.query;
            const projects = await ProjectModel.find({year, queries})

            return res.json(projects)
        }
    } catch (err) {
        res.status(500).send(err);
    }
}