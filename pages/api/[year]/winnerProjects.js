import ProjectModel from "../../../backend/db/models/Project";

export default async function WinnerProjects(req, res) {
    try {
        if (req.method === 'POST') {
            const projects = await ProjectModel.findExistKeys(req.body, {year: req.query.year});

            return res.json(projects);
        }
    } catch (err) {
        res.status(500).send(err);
    }
}