import InternshipModel from "../../../backend/db/models/Internship";
import InstructorModel from "../../../backend/db/models/Instructor";
import ProjectModel from "backend/db/models/Project";

export default async function DataByYear(req, res) {
    try {
        const { year } = req.query;
        const internships = await InternshipModel.find({ year });
        const instructors  = await InstructorModel.find({ year });

        return res.json({
            internships,
            instructors,
        });
    } catch (err) {
        res.status(500).send(err);
    }
};