import { getYears } from "../../../backend/utils/years";
import WinnerProjectTypesModel from "../../../backend/db/models/WinnerProjectTypes";

export default async function getMetadata(req, res) {
    try {
        const currentYear = req.query.year
        const yearData = getYears(currentYear);
        const winningProjectTypes = await WinnerProjectTypesModel.find();

        return res.json({
                winningProjectTypes,
                ...yearData
        })
    } catch (err) {
        console.log(err);
    }
}