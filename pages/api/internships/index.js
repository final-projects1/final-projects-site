import InternshipModel from "../../../backend/db/models/Internship";

export default async function internships(req, res) {
    try {
        const data = await InternshipModel.find();

        return res.json(data)
    } catch (err) {
        res.status(500).send(err);
    }
}