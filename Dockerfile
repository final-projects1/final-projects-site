FROM node:lts-alpine3.14

RUN mkdir -p /app

WORKDIR /app

COPY . .

RUN npm install
RUN npm run build:projects
RUN npm run build

CMD ["npm", "start"]