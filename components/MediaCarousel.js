import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import { sliderData } from '../db/sliderData';
import Video from './Video';

import styles from 'styles/Video.module.css';

function MediaCarousel() {
  return (
    <Carousel className={styles['carousel-container']} centerMode={true} showThumbs={false} showArrows={false} showStatus={false} autoPlay={true} infiniteLoop={true}>
      {sliderData.map((slide, index) => {
        return (
          <div key={index}>
            <Video key={index} src={slide.image} alt="slide" />
          </div>
        );
      })}
    </Carousel>
  );
}

export default MediaCarousel;
