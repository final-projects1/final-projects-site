import PropTypes from 'prop-types';

import Image from './Image';

function Logo({ src, width, height, alt }) {
  return (
    <>
      <Image src={src} width={width} height={height} alt={alt} />
    </>
  );
}

Logo.defaultProps = {
  src: '/images/colman_logo.png',
  alt: 'colman_logo',
  width: 130,
  height: 130,
};

Logo.propTypes = {
  src: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  alt: PropTypes.string,
};

export default Logo;
