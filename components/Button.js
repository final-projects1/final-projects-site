import PropTypes from 'prop-types';

import styles from 'styles/Button.module.css';

function Button({ children, onClick, className, type, theme, disabled }) {
  return (
    <button
      onClick={onClick}
      className={`${className} ${styles.button}`}
      type={type}
      theme={theme}
      disabled={disabled}
    >
      {children}
    </button>
  );
}

Button.defaultProps = {
  disabled: false,
  type: 'button',
  className: '',
};

Button.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string,
  type: PropTypes.string,
  theme: PropTypes.string,
  disabled: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

export default Button;
