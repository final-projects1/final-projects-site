import Text from 'components/Text';

import styles from 'styles/Footer.module.css';

function Footer() {
  return (
    <div className={styles['footer-container']}>
      <Text className={styles['footer-text']}>
        For registration and information <span className={styles['text-underline']}>click here</span>
      </Text>
    </div>
  );
}

export default Footer;
