import PropTypes from 'prop-types';

import styles from 'styles/List.module.css';

function ListItem({ children, className }) {
  return <li className={`${styles.listItem} ${className}`}>{children}</li>;
}

ListItem.defaultProps = {
  className: '',
};

ListItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),

    PropTypes.node,
  ]).isRequired,
  className: PropTypes.string,
};

export default ListItem;
