import PropTypes from 'prop-types';

import styles from 'styles/List.module.css';

import ListItem from './ListItem';

function List({ items, isOrdered, className, itemsClass }) {
  const Component = isOrdered ? 'ol' : 'ul';

  return (
    <Component className={`${styles.list} ${className}`}>
      {items.map((item, index) => (
        <ListItem key={`${index}`} className={itemsClass}>
          {item}
        </ListItem>
      ))}
    </Component>
  );
}

List.defaultProps = {
  isOrdered: false,
  className: '',
};

List.propTypes = {
  items: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  isOrdered: PropTypes.bool,
  className: PropTypes.string,
  itemsClass: PropTypes.string,
};

export default List;
