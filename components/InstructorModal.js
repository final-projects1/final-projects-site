import React from 'react';
import Heading from './Heading';
import Text from './Text';
import Image from './Image';

import styles from 'styles/InstructorModal.module.css';

function InstructorModal({ title, description, imgName, path}) {

  return (
    <div className={styles['modal-container']}>
      <div>
        <Image imgName={imgName} path={path} alt={title} layout={"fixed"} width={"200px"} height={"260px"} />
     </div> 
     <div className={styles['modal-section']}>
        <Heading level="2">{title} </Heading>
        <Text as="p">{description} </Text>
      </div>
    </div>
  );
}

export default InstructorModal;
