import { useState } from "react";
import { MenuItem, TextField } from "@mui/material";

function Dropdown({ name, items, register }) {
  const [selected, setSelected] = useState({name: "", value: ""});

  return (
    <TextField
      select
      value={selected.name}
      label={name}
      {...register(name, { required: true })}
    >
      {items.map(({ value, name }, index) => (
        <MenuItem key={index} onClick={()=>setSelected({name,value})} value={value}>
          {name}
        </MenuItem>
      ))}
    </TextField>
  );
}
export default Dropdown;
