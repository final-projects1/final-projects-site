import React from "react";
import { useForm } from "react-hook-form";
import { Button, TextField } from "@mui/material";
import styles from "styles/FormToJson.module.css";
import Dropdown from "./Form/Dropdown";
import { useState } from "react";
import ImageUpload from "./Form/ImageUpload";
;

function FormToJson({ formData }) {
  const { handleSubmit, register } = useForm();

  const saveData = (data) => {
    console.log(data);
  };
  const [selected, setSelected] = useState("");

  const handleChange = (event) => {
    setSelected(event.target.value);
  };

  return (
    <form
      onSubmit={handleSubmit(saveData)}
      className={styles["form-container"]}
    >
      {formData.map(
        ({ Component, name, items, placeholder, isDropdown }, index) => (
          <>
            {isDropdown ? (
              <Component
                key={index}
                name={name}
                items={items}
                register={register}
              />
            ) : (
              <TextField
                key={index}
                placeholder={placeholder}
                variant="outlined"
                name={name}
                {...register(name, { required: true })}
              />
            )}
          </>
        )
      )}
      <ImageUpload/>
      <Button variant="outlined" type="submit">
        Submit
      </Button>
    </form>
  );
}
export default FormToJson;
