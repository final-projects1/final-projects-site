import NextImg from 'next/image';
import PropTypes from 'prop-types';
import useResources from "hooks/useResources";
import { useSelector } from 'react-redux';

const Image = ({ imgName, alt, className, height, width, layout, path, src }) => {
  const currentYear = useSelector(({ metaData }) => metaData.currentYear);
  const currSrc = useResources(path, imgName, currentYear);

  return (
    <NextImg
      className={className}
      src={src || currSrc}
      alt={alt}
      height={height}
      width={width}
      layout={layout}
    />
  );
};

Image.defaultProps = {
  layout: 'intrinsic',
};

Image.propTypes = {
  className: PropTypes.string,
  layout: PropTypes.string,
  path: PropTypes.string,
  imgName: PropTypes.string,
  src: PropTypes.object,
  alt: PropTypes.string.isRequired,
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default Image;
