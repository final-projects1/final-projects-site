import PropTypes from 'prop-types';

import styles from 'styles/Container.module.css';

function Container({ children, className }) {
  return <div className={`${styles.container} ${className}`}>{children}</div>;
}

Container.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};
export default Container;
