import styles from 'styles/Form.module.css' 

function Form() {
    const registerUser = async event => {
      event.preventDefault()
  
      const res = await fetch('/api/register', {
        body: JSON.stringify({
          firstName: event.target.firstName.value,
          lastName :  event.target.lastName.value,
          phone: event.target.phone.value,
          email: event.target.email.value,
        }),
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST'
      })
  
      const result = await res.json()
    }
  
    return (
      <form onSubmit={registerUser} className={styles['form-container']}>
        <input id="firstName" name="firstName" type="text" placeholder="First Name" className={styles['form-item']} required />
        <input id="lastName" name="lastName" type="text" placeholder="Last Name" className={styles['form-item']} required />
        <input id="phone" name="phone" type="text" placeholder="Phone" className={styles['form-item']} required />
        <input id="email" name="email" type="email" placeholder="Email" className={styles['form-item']} required />
        <button type="submit" className={styles['submit']} >Register</button>
      </form>
    )
  }
  export default Form;