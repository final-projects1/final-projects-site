import { useRef, useEffect } from 'react';
import styles from '../styles/Navbar.module.css';
import { shallowEqual, useSelector } from 'react-redux';
import useNavbarLinks from 'hooks/useNavbarLinks';
import Link from 'next/link';

function Navbar() {
  const [isNavbarHidden, currentYear] = useSelector(
    ({ metaData }) => ([metaData.hideNavbar, metaData.currentYear]),
    shallowEqual,
  );
  const links = useNavbarLinks();
  const mobileNavbar= useRef();

  const closeNavbar = () => {
    mobileNavbar.current.checked=false 
  };

  useEffect(() => {
    window.addEventListener('resize', closeNavbar);
    return () => window.removeEventListener('resize', closeNavbar);
  }, [closeNavbar]);

  return (
    !isNavbarHidden &&
    links?.length && (
      <div className={styles['navbar-container']}>
        <Link href={`/${currentYear}`}>
          <a>
            <img
              className={styles['navbar-img']}
              src="/images/colman_logo.png"
              alt="colman logo"
            />
          </a>
        </Link>
        <div className={styles['navbar-wrapper']}>
          {links.map(
            ({ links, title, href, Component, options, defaultTitle }) => {
              return (
                <Component
                  key={title}
                  href={href}
                  links={links}
                  title={title}
                  options={options}
                  defaultTitle={defaultTitle}
                />
              );
            },
          )}
        </div>
        <input id={styles['hambCheckbox']} type="checkbox" ref={mobileNavbar} />
        <div className={styles['navbar-hamburger']}>
          <label
            className={styles['hamburger-lines']}
            htmlFor={styles['hambCheckbox']}
          >
            <span className={`${styles['line']} ${styles['line1']}`} />
            <span className={`${styles['line']} ${styles['line2']}`} />
            <span className={`${styles['line']} ${styles['line3']}`} />
          </label>
        </div>
        <div
          className={`${styles['menu-items']} ${styles['navbar-mobile-open']}`}
          onClick={closeNavbar}
        >
          {links.map(
            ({ links, title, href, Component, options, defaultTitle }) => {
              return (
                <Component
                  key={title}
                  href={href}
                  links={links}
                  title={title}
                  options={{...options, closeNavbar}}
                  defaultTitle={defaultTitle}
                />
              );
            },
          )}
        </div>
      </div>
    )
  );
}
export default Navbar;
