/* eslint no-restricted-imports: "off" */
import NextLink from 'next/link';
import PropTypes from 'prop-types';

import styles from 'styles/Link.module.css';
import {useRouter} from "next/router";

function Link({ href, title, className, internal, target, children}) {
  const { query: { year='' } } = useRouter();
  return internal ? (
    <NextLink href={{
      pathname: `${year ? `/[year]${href}` : href}`,
      query: { year }
    }}>
      <a islink={!!title && 'underline'} className={`${styles.hyperlink} ${className}`} target={target}>
        {title || children}
      </a>
    </NextLink>
  ) : (
    <a
      className={`${styles.hyperlink} ${className}`}
      href={href}
      target={target}
    >
      {title}
    </a>
  );
}

Link.defaultProps = {
  target: '_self',
  className: '',
  internal: true,
};

Link.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string.isRequired,
  target: PropTypes.oneOf(['_blank', '_self', '_parent', '_top']),
  internal: PropTypes.bool,
  title: PropTypes.string,
};

export default Link;




