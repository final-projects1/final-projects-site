import styles from 'styles/Video.module.css';

function Video({ src }) {
  return (
      <iframe
      className={styles['video-frame']}
        src={src}
        title="YouTube video player"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
  );
}

export default Video;
