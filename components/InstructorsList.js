
import InstructorCard from './InstructorCard';
import PropTypes from 'prop-types';
import styles from 'styles/InstructorsList.module.css';

function InstructorsList({ instructors }) {
  return (
    <>
      <div className={styles['instructors-container']}>
      {instructors && instructors.map((instructor, index) => (
        <InstructorCard
          key={`${instructor.id}${index}`}
          imgName={instructor.image}
          title={instructor.name}
          description={instructor.description}
        />
      ))}
    </div>
</>
  );
}

InstructorsList.propTypes = {
  instructors: PropTypes.array.isRequired,
};

export default InstructorsList;
