import ProjectCard from 'components/ProjectCard';
import PropTypes from 'prop-types';
import Heading from './Heading';

import styles from 'styles/ProjectList.module.css';
import {shallowEqual, useSelector} from "react-redux";

function ProjectList({ projects, title }) {
  const winningProjectTypes = useSelector(({ metaData: { winningProjectTypes } }) => winningProjectTypes, shallowEqual);

  return (
    <>
      {title && (
        <Heading level="2" className={styles['projects-header']}>
          {title}
        </Heading>
      )}
      <div className={styles['projects-container']}>
        {projects &&
          projects.map((project, index) => (
            <ProjectCard
              key={`${project.projectId}${index}`}
              id= {project.projectId}
              imgName={project.image}
              title={project.name}
              description={project.description}
              instructorId={project.instructorId}
              members={project.members}
              projectWinner={(winningProjectTypes && project.projectWinnerId) && winningProjectTypes.find(({ id }) => id === project.projectWinnerId)}
            />
          ))}
      </div>
    </>
  );
}

ProjectList.propTypes = {
  projects: PropTypes.array,
};

export default ProjectList;
