import PropTypes from 'prop-types';

import styles from 'styles/Text.module.css';

function Text({ children, className, as: Component }) {
  return (
    <Component className={`${className} ${styles.text}`}>{children}</Component>
  );
}

Text.defaultProps = {
  as: 'p',
};

Text.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  className: PropTypes.string,
  as: PropTypes.oneOf(['p', 'div', 'span', 'quote', 'q']),
};

export default Text;
