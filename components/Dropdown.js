import { useCallback, useEffect, useRef, useState } from 'react';
import styles from 'styles/Dropdown.module.css';

const FADE_TIMEOUT = 300;

function Dropdown({ title, links, defaultTitle, options }) {
  const [isOpen, setIsOpen] = useState(false);
  const [currentOption, setCurrentOption] = useState(defaultTitle || title);
  const dropdownLink = useRef();


  const onFadeOut = useCallback(currDropdownState => {
    setTimeout(
      () => setIsOpen(!currDropdownState),
      currDropdownState ? FADE_TIMEOUT : 0,
    );
  }, []);

  const onFadeIn = useCallback(
    () =>
      setTimeout(() => {
        dropdownLink.current &&
          (dropdownLink.current.attributes.action.value = 'open');
      }, FADE_TIMEOUT),
    [],
  );

  const onToggle = useCallback(
    e => {
      isOpen &&
        dropdownLink.current &&
        (dropdownLink.current.attributes.action.value = 'close');
      e.stopPropagation();
      onFadeOut(isOpen);
    },
    [isOpen],
  );

  useEffect(() => isOpen && dropdownLink.current && onFadeIn(), [isOpen]);

  const closeNav = () => {
    if(options?.closeNavbar)
      options.closeNavbar();
  }

  const handleOnClick = useCallback(
    (option, onClick) => {
      if (onClick && option !== currentOption) {
        onClick(option);
        setCurrentOption(option);
      }
    },
    [currentOption],
  );

  return (
    <div className={styles['dropdown-list']} onClick={e => onToggle(e)}>
      {isOpen && (
        <div className={styles['dropdown-overlay']} />
      )}
      <p className={styles['primary-link']}>{title} </p>
      {isOpen ? (
        <div
          ref={dropdownLink}
          action="close"
          className={styles['dropdown-links']}
          onClick={closeNav}
        >
          {links.map(
            ({
              links,
              title,
              href,
              Component,
              options: { onClick, ...rest },
            }) => (
                    <Component
                        key={title}
                        href={href}
                        links={links}
                        title={title}
                        onClick={() => handleOnClick(title, onClick)}
                        shallow
                        {...rest}
                    >
                        {title}
                    </Component>
                ),
          )}
        </div>
      ) : null}
    </div>
  );
}

export default Dropdown;
