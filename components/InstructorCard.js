import { useCallback } from 'react';
import Heading from 'components/Heading';
import Image from 'components/Image';
import PropTypes from 'prop-types';

import styles from 'styles/InstructorCard.module.css';
import { useDispatch } from 'react-redux';
import { openModal } from '../redux/reducers/modal/actions';
import { INSTRUCTOR_MODAL } from '../enums/modals';
function InstructorCard({ imgName, title, description }) {
  const dispatch = useDispatch();
  

  const onOpenInstructorModal = useCallback(
    () =>
      dispatch(
        openModal({
          type: INSTRUCTOR_MODAL,
          data: { title, description, imgName, path: "instructors"},
        }),
      ),
    [],
  );

  return (
    <div
      className={styles['instructor-card-container']}
      onClick={onOpenInstructorModal}
    >
      <div className={styles['second-img-border']}>
        <div className={styles['img-border']}>
          <Image imgName={imgName} path="instructors" alt={title} layout="fill" />
        </div>
      </div>
      <Heading level="3" className={styles['instructor-card-title']}>
        {title}
      </Heading>
    </div>
  );
}

InstructorCard.propTypes = {
  title: PropTypes.string.isRequired,
  src: PropTypes.string,
  description: PropTypes.string.isRequired,
};

export default InstructorCard;
