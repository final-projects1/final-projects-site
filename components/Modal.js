import {useMemo, useState, forwardRef} from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Backdrop from '@mui/material/Backdrop';
import InstructorModal from './InstructorModal';
import {useDispatch, useSelector} from "react-redux";
import {closeModal} from "../redux/reducers/modal/actions";
import { useSpring, animated } from '@react-spring/web';

const Fade = forwardRef(function Fade(props, ref) {
  const { in: open, children, onEnter, onExited, ...other } = props;
  const style = useSpring({
    from: { opacity: 0 },
    to: { opacity: open ? 1 : 0 },
    onStart: () => {
      if (open && onEnter) {
        onEnter();
      }
    },
    onRest: () => {
      if (!open && onExited) {
        onExited();
      }
    },
  });

  return (
      <animated.div ref={ref} style={style} {...other}>
        {children}
      </animated.div>
  );
});

Fade.propTypes = {
  children: PropTypes.element,
  in: PropTypes.bool.isRequired,
  onEnter: PropTypes.func,
  onExited: PropTypes.func,
};

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  padding: "8px 5px",
};

const modalComponents = {
  instructor: InstructorModal
}

export default function BasicModal() {
  const dispatch = useDispatch();
  const { isOpen, type, data } = useSelector(({ modal }) => modal);
  const ModalComponent = useMemo(() => modalComponents[type], [type]);

  return (
    <>
      {isOpen && (<div>
          <Modal
            open={isOpen}
            onClose={() => dispatch(closeModal())}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 300,
            }}
          >
            <Fade in={isOpen}>
              <Box sx={style}>
                <ModalComponent { ...data } />
              </Box>
            </Fade>
          </Modal>
        </div>)}
    </>
  );
}
