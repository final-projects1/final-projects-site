import PropTypes from 'prop-types';

import styles from 'styles/Input.module.css';

const Input = ({
  placeholder,
  value,
  className,
  label,
  onClick,
  onChange,
  name,
  type,
}) => {
  return (
    <label className={`${styles.label} ${className}`}>
      {label}
      <input
        className={`${styles.input} ${className}`}
        name={name}
        placeholder={placeholder}
        value={value}
        onClick={onClick}
        onChange={onChange}
        type={type}
      />
    </label>
  );
};

Input.propTypes = {
  onClick: PropTypes.func,
  onChange: PropTypes.func,
  className: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
};

export default Input;
