import Arrow from 'components/icons/ArrowLeft';
import Image from 'components/Image';
import Text from 'components/Text';
import PropTypes from 'prop-types';
import styles from 'styles/ProjectCard.module.css';
import { GiArchiveResearch, GiPodiumWinner } from 'react-icons/gi';
import Tooltip from '@mui/material/Tooltip';
import { useRouter } from 'next/router';
import {useMemo} from "react";
import Link from "components/Link";

const iconsWinners = {
    1: GiPodiumWinner,
    2: GiArchiveResearch
}

function ProjectCard({
  id,
  title,
  imgName,
  projectWinner
}) {
  const Icon = useMemo(() => projectWinner && iconsWinners[projectWinner.id], [projectWinner])

  return (
      <Link href={`/projects/${id}`} internal>
        <div className={styles['project-card-container']}>
            {(projectWinner && Icon) &&
            <Tooltip title={projectWinner.name}>
                <span theme={projectWinner.id} className={styles['project-icon_winner']}>{Icon()}</span>
            </Tooltip>}
          <Image
            imgName={imgName}
            path="projects"
            className={styles['project-card-img']}
            alt={title}
            layout="fill"
          />
          <Text as="p" className={styles['project-card-title']}>
            <Text as="span">{title} </Text>
            <Arrow
              className={styles['project-card-arrow']}
              width={25}
              height={25}
            />
          </Text>
        </div>
      </Link>
  );
}

ProjectCard.propTypes = {
  title: PropTypes.string.isRequired,
  src: PropTypes.string,
};

export default ProjectCard;
