const dbPath = `${process.cwd()}/db`;
import  fs from 'fs';
import path from 'path';
const _getPath = (fileName, year) => path.join(dbPath, `${year?year:''}`,`${fileName}.json`);

const _methods = {
    exist: (item, key) => !!item[key],
}

const _checkFilter = (item, filters) => {
    let isValid = true;
    for(let [key, val] of filters) {
        if (!(item[key] && item[key] == val)) {
            isValid = false;
            break;
        }
    }
    return isValid;
}

/**
 * Create Model
 * @public
 *
 * @param {string} modelName
 *
 * @param {Object} scheme
 */

export default function Model(modelName) {
    return {
        async find(options={}) {
            try {
                const { queries, year=null, method=null } = options;
                const data = fs.readFileSync(_getPath(modelName, year));
                let parsedData = JSON.parse(data);

                if (queries) {
                    const filters = Object.entries(queries);
                    parsedData = parsedData.filter(item => _checkFilter(item, filters, method));
                }

                return parsedData;
            } catch (err) {
                throw new Error(`failed finding ${modelName}`, err);
            }
        },
        async findExistKeys(arrExistKeys=[], options) {
            try {
                if (!Array.isArray(arrExistKeys) || !arrExistKeys.length) throw new Error('must be array of strings');
                const { year } = options;

                let data = await this.find({year});

                data = data.filter(item => !!arrExistKeys.find(key => !!item[key]));

                return data
            } catch (err) {
                throw new Error(`failed`, err);
            }
        },
        async insert(model, options) {
            try {
                // const model = scheme(dataModel);
                const data = await this.find(options);
                data.push(model);

                await fs.writeFileSync(_getPath(modelName), JSON.stringify(data));

                return model;
            } catch (err) {
                throw new Error(`failed writing into ${modelName}`, err);
            }
        },
        async put({key, value}, model, options) {
            try {
                // const model = scheme(dataModel);
                const data = await this.find(options);
                let foundOne = data.find(item => item[key] === value);

                if (!foundOne) throw new Error(`${modelName} item not exist`);

                foundOne = {...foundOne, ...model};

                const newDataList = data.filter(({id}) => foundOne.id !== id);
                newDataList.push(foundOne);

                fs.writeFileSync(_getPath(modelName), JSON.stringify(newDataList));

                return foundOne;
            } catch (err) {
                throw new Error(`failed put by key: ${key} in ${modelName}, data: ${JSON.parse(modelData)}`, err);
            }
        }
    }
}