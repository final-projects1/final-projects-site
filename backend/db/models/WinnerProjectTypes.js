import { IMAGE_PATTERN } from '../../enums';
import Ajv from 'ajv/dist/jtd';
import Model from "../index";
const ajv = new Ajv();

const schema = {
    properties: {
        id: { type: "number" },
        name: { type: "string" },
        icon: { type: "string", pattern: IMAGE_PATTERN }
    }
}

// const validate = ajv.compile(schema);

const WinnerProjectTypesModel = Model('winnerProjectTypes');

export default WinnerProjectTypesModel;
