import Model from '../index';
import { IMAGE_PATTERN } from '../../enums';
import Ajv from 'ajv/dist/jtd';
import { defaultDescriptionScheme, defaultNameScheme } from "../../utils/schemes";
const ajv = new Ajv();

const schema = {
    properties: {
        id: { type: "string" },
        ...defaultNameScheme,
        image: { type: "string", pattern: IMAGE_PATTERN },
    },
    optionalProperties: {
        ...defaultDescriptionScheme
    }
}

const InstructorModel = Model('instructors');

export default InstructorModel;
