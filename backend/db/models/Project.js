import Joi from 'joi';
import { IMAGE_PATTERN } from '../../enums/index.js';
import Model from "../index.js";

export const Schema = Joi.object({
    id: Joi.number().required(),
    projectId: Joi.string().required(),
    name: Joi.string().required(),
    image: Joi.string().pattern(IMAGE_PATTERN).allow(""),
    members: Joi.array()
        .min(1)
        .max(6)
        .items(
            Joi.object({
                name: Joi.string().required(),
                email: Joi.string().email().required()
            }).required()
    ),
    instructorId: Joi.string().required(),
    internshipId: Joi.string().required(),
    projectWinnerId: Joi.string().optional(),
    description: Joi.string().max(1000).optional()
})

// const validate = ajv.compile(schema);

const ProjectModel = Model('projects');

export default ProjectModel;
