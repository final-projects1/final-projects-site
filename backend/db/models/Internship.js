import Model from '../index';
import Ajv from 'ajv/dist/jtd';
const ajv = new Ajv();
import { IMAGE_PATTERN } from '../../enums';
import { defaultNameScheme, defaultDescriptionScheme } from "../../utils/schemes";

const schema = {
    properties: {
        id: { type: "string" },
        ...defaultNameScheme,
        image: { type: "string", pattern: IMAGE_PATTERN },
        key: { type: "string" }
    },
    optionalProperties: {
        ...defaultDescriptionScheme,
        instructors: {
            type: 'array',
            items: {
                type: "string"
            }
        }
    }
}

// const validate = ajv.compile(schema);

const InternshipModel = Model('internships');

export default InternshipModel;
