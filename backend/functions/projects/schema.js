const Joi =  require('joi');

module.exports = Joi.object({
    id: Joi.number().required(),
    projectId: Joi.string().required(),
    name: Joi.string().required(),
    image: Joi.string().pattern(/\.(jpe?g)$/i).allow(""),
    members: Joi.array()
        .min(1)
        .max(6)
        .items(
            Joi.object({
                name: Joi.string().required(),
                email: Joi.string().email().required()
            }).required()
        ),
    instructorId: Joi.string().required(),
    internshipId: Joi.string().required(),
    projectWinnerId: Joi.string().optional(),
    description: Joi.string().max(1000).optional()
})