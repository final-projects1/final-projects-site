const {statSync, readdirSync, readFileSync, writeFileSync, copyFileSync, constants, accessSync} = require('fs');
const path = require('path');
const Schema = require('./schema');
const { Logger } = require('../../utils/logger');

const projectsLogger = Logger('projects');
const year = new Date().getFullYear().toString();

function checkFileExists(filepath) {
    let flag = true;

    try {
        accessSync(filepath, constants.F_OK);
    } catch(e){
        flag = false;
    }

    return flag;
}

function getDirectories(path) {
    return readdirSync(path).filter(function (file) {
        return statSync(path+'/'+file).isDirectory();
    });
}

function copyImage(source, imgName) {
    const target = path.join(process.cwd(), 'db', year, 'storage', 'projects');

    copyFileSync(`${source}/${imgName}`, `${target}/${imgName}`,constants.COPYFILE_FICLONE);
}

function createProjects(projects, currentPath) {
    const newProjectsList = [];

    projects.forEach((projectId, index) => {
        try {
            const currentDirectory = `${currentPath}/${projectId}`;
            const imageExist = checkFileExists(`${currentDirectory}/${projectId}.jpg`);

            const parsedJson = JSON.parse(readFileSync(path.join(`${currentDirectory}/${projectId}.json`)));
            parsedJson.id = index+1;
            !imageExist && (parsedJson.image = "");

            const finalJson = Schema.validate(parsedJson);

            if (finalJson.error) throw finalJson.error;

            newProjectsList.push(finalJson.value);

            copyImage(currentDirectory, finalJson.value.image)
        } catch (err) {
            projectsLogger(`${new Date().toISOString()}: project id ${projectId} failed to built: ${err.message}`)
        }
    })

    return newProjectsList;
}

function run () {
    projectsLogger('----- start building projects ------')

    try {
        const entrypointPath = path.join(process.cwd(), 'drafts', year, 'projects');

        const projects = getDirectories(entrypointPath);
        const newProjectsArr = JSON.stringify(createProjects(projects, entrypointPath));

        projectsLogger('start writing new projects');
        writeFileSync(path.join(process.cwd(), 'db', year, 'projects.json'), newProjectsArr);
        projectsLogger('end writing new projects');

    } catch (err) {
        projectsLogger(JSON.stringify(err));
    }

    projectsLogger('----- finish building projects successfully ------ \n\n')
}

run();