export const defaultNameScheme = {
    name: {
        type: "string",
        minLength: 2,
        maxLength: 25
    }
}

export const defaultDescriptionScheme = {
    description: { type: "string", maxLength: 255 }
}