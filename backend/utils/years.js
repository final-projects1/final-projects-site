import fs from 'fs';
import path from 'path';
const firstYear = 2021;
const currentYear = Number(process.env.CURRENT_YEAR);


export function getYears(currentYear) {
    try {
        const dbDirectoryPath = path.join(process.env.INIT_CWD, 'db');
        const dirs = fs.readdirSync(dbDirectoryPath).filter(dirname => !isNaN(dirname) && firstYear <= Number(dirname));

        return {
            years: dirs,
            currentYear: _getCurrentYear(dirs, Number(currentYear))
        }
    } catch (err) { }
}

function _getCurrentYear(years, currentYear) {
    let year = years.find(itemYear => currentYear && Number(itemYear) === currentYear) || new Date().getFullYear();

    return year;
}