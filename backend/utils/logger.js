const { appendFile } = require('fs');
const path = require('path');

module.exports = {
    Logger
};

function Logger(file) {
    return log => appendFile(path.join(process.cwd(), 'logs', `${file}.txt`),
        `${new Date().toISOString()} ${log + '\n'}`,
        function () {});
}